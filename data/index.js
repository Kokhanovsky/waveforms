const express = require('express');
const app = express();
const port = 3434;

let data = require('./arr.json');

let time = getTime(data);
console.log("Total time:", time.toFixed(0), "minutes");

app.get('/waveforms.json', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.json(selectRange(data, req));
});

app.listen(port, function () {
    console.log('The application listening on port', port);
});

function getTime(data) {
    return data.length / 10 / 60;
}

function selectRange(data, req) {
    if (!req.query.time) {
        return [];
    }
    let timeArr = req.query.time.split(",").map(item => item * 10);
    if (timeArr.length < 2) {
        return [];
    }
    return data.slice(timeArr[0], timeArr[1] + 1);
}
