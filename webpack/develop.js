const webpack = require("webpack"),
    CopyWebpackPlugin = require("copy-webpack-plugin"),
    HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    devtool: 'intline-source-map',

    entry: "./src/main.js",

    module: {

        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                loader: "html-loader",
                exclude: /node_modules/
            },

        ],

    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new HtmlWebpackPlugin({
            template: "./src/demo/index.html",
            inject: true
        }),
    ],

    devServer: {
        open: true,
        hot: true,
        disableHostCheck: true,
        port: 8090,
        stats: 'errors-only'
    },

};
