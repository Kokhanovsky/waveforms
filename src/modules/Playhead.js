import * as Konva from "konva";
import events from "./Events"

/**
 * Class representing the Playhead
 */
export default class Playhead {

    blockDuration;
    containerWidth;
    containerHeight;
    trackTime;
    zoom;
    stage;

    constructor(options) {
        Object.assign(this, options);

        this.containerWidth = this.stage.getWidth();
        this.containerHeight = this.stage.getHeight();

        this.layer = new Konva.Layer({
            draggable: true
        });
        this.draw();

        this.layer.on("dragmove", () => {
            this.keepInBounds();
            events.emit('stop');
        });

        events.on("blocksMove", data => {
            this.setXPosition(data.event.evt.movementX);
            this.keepInBounds();
        });

        /**
         * Play animation for this.layer Playhead
         * @type {Konva.Animation}
         */
        this.animation = new Konva.Animation(frame => {
            let dist = this.containerWidth * frame.timeDiff / 1000 / this.blockDuration  * this.zoom ;
            this.setXPosition(dist);
            let k = this.keepInBounds();
            if (!k) {
                this.animation.stop();
                events.emit('stop');
            }
        }, this.layer);

        events.on("play", () => {
            console.log('play');
        });

        events.on("stop", () => {
            this.animation.stop();
            console.log('stop');
        });

        events.on('startPlayhead', () => {
            this.animation.start();
        })


    }

    keepInBounds() {
        let posX = this.getXPosition();
        let x = posX < 0 ? 0 : posX > this.containerWidth ? this.containerWidth : posX;
        this.layer.position({x});
        this.layer.draw();
        return (x >= 0 && x < this.containerWidth);
    }

    draw() {
        let self = this;
        let playheadShape = new Konva.Shape({
            opacity: 1,
            strokeWidth: 14,
            fill: '#000',
            sceneFunc: function (context) {
                self.drawPlayHead(context, this);
            }
        });
        this.layer.add(playheadShape).moveToTop();
        this.layer.offsetX(10);
        this.stage.add(this.layer);

        this.layer.on("dragmove", (e) => {
            //this.getPosition();
        });
    }

    /**
     * Draw playhead
     * @param context {Konva.SceneContext}
     * @param self {Konva.Shape}
     */
    drawPlayHead(context, self) {
        context.beginPath();
        context.lineTo(0, this.containerHeight);
        context.lineTo(10, this.containerHeight - 10);
        context.lineTo(20, this.containerHeight);
        context.stroke();
        context.closePath();
        context.fillStrokeShape(self);
        context.beginPath();
        context.lineTo(9, 0);
        context.lineTo(9, this.containerHeight);
        context.lineTo(11, this.containerHeight);
        context.lineTo(11, 0);
        context.closePath();
        context.fillStrokeShape(self);
    }

    /**
     * Get current position of Playhead in seconds
     */
    getXPosition() {
        let x = this.layer.position().x;
        return x;
    }

    /**
     * Set X position for the Playhead
     */
    setXPosition(delta) {
        let posX = this.getXPosition() + delta;
        this.layer.position({x: posX});
        this.layer.draw();
    }

    setPosition(x) {
        this.layer.position({x});
        this.layer.draw();
    }

    /**
     * Get current play position in seconds
     */
    getPlayheadPosition() {

    }

}
