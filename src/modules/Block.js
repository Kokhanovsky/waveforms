import Events from "./Events"


/**
 * Class representing audio track single block
 */
export default class Block {

    x;
    width;
    height;
    backgroundColor;
    /**
     * the block time range in seconds [t1, t2]
     */
    timeRange;

    /**
     * @property {Konva.Shape} this.shape The block Konva shape
     */
    shape;

    /**
     * @property {Array} this.data This contains waveforms data for the block
     */
    data = [];

    /**
     * Creates a block
     * @param {Object} options
     * @param {number} options.x Position x
     * @param {number} options.width Block width in pixels
     * @param {number} options.height Block height in pixels
     * @param {number} options.timeRange block time range the first value is a time start the second value is the end of the range
     */
    constructor(options) {
        Object.assign(this, options);
        this.create();
    }

    create() {
        this.group = new Konva.Group();
        let background = new Konva.Rect({
            x: this.x,
            width: this.width,
            height: this.height,
            fill: this.backgroundColor
        });
        this.group.add(background);
        this.requestData();
    }

    requestData() {
        $.getJSON("//" + window.location.hostname + ":3434/waveforms.json?time=" + this.timeRange.join(','), data => {
            this.draw(data);
        });
    }

    draw(data) {
        let self = this;
        let waveforms = new Konva.Shape({
            opacity: 1,
            strokeWidth: 0,
            fill: '#fefffb',
            sceneFunc: function (context) {
                context.beginPath();
                context.strokeStyle = '#ffffff';
                let count = 0;
                context.lineTo(self.x, self.height);
                let step = self.width / (data.length - 1) ;
                data.forEach((d, index) => {
                    context.lineTo(index * step + self.x, self.height - d);
                    count = index;
                });
                context.lineTo(count * step + self.x , self.height);
                context.stroke();
                context.closePath();
                context.fillStrokeShape(this);

            }
        });

        this.group.add(waveforms);
        waveforms.draw();

    }



}
