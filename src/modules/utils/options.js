import Konva from "konva"

const defaultOptions = {
    width: 600,
    height: 100
};

export default function (options) {
    return { ...defaultOptions, ...options };

}
