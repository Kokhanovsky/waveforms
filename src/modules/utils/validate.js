export default function(wf) {
    if (!(wf.options.id || !wf.options.element)) {
        console.warn("Empty element or element id");
        return false;
    }
    if (wf.options.element) {
        if (!wf.options.element instanceof Element) {
            console.warn("It is not a DOM element.");
            return false;
        }
        wf.element = wf.options.element;
    }
    if (wf.options.id) {
        wf.element = document.getElementById(wf.options.id);
        if (!wf.element) {
            console.warn("Can't find a DOM element.");
            return false;
        }
    }
    return true;
}
