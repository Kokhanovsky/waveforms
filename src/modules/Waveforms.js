import Konva from 'konva'
import events from './Events'
import setOptions from "./utils/options"
import validate from './utils/validate'
import Blocks from "./Blocks";


const _initStage = Symbol('_initStage');
const _blocks = Symbol('_blocks');
const _attachEvents = Symbol('_attachEvents');



export default class wf {
    /**
     * The DOM element to which canvas is attached
     */
    element;
    blocks;
    trackTime;
    /**
     * Duration of the block
     */
    blockDuration;

    constructor(options) {
        Object.assign(this, options);
        this.options = setOptions(options); // todo: review it
        this.events = events;
        if (!validate(this)) { // todo: review this function
            return;
        }

        this[_initStage](); // create main container

        this[_blocks] = new Blocks({
            blockDuration: this.blockDuration, // in seconds
            stage: this.stage,
            trackTime: this.trackTime <= this.blockDuration ? this.blockDuration : this.trackTime,
            zoom: 1 // must be greater than one
        });

        this[_blocks].update({
            containerWidth: 100,
            containerHeight: 100
        });



        this[_attachEvents]();
    }

    // Private methods

    [_initStage]() {
        this.stage = new Konva.Stage({
            container: this.element,
            width: this.options.width,
            height: this.options.height
        });
    }

    [_attachEvents]() {
        this.play = () => events.emit("play");
        this.stop = () => events.emit("stop");
        this.seek = time => events.emit("seek", time);
        this.zoomIn = () => events.emit("zoomIn");
        this.zoomOut = () => events.emit("zoomOut");
        this.goStart = () => events.emit("goStart");
        this.goEnd = () => events.emit("goEnd");
    }


}
