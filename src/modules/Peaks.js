import Zoom from "./Zoom"

export default class Peaks {

    layers = [];

    constructor(wf) {
        this.wf = wf;
        this.samples = this.normalize(this.wf.data.wav_samples);
        this.pixelsPerSecond = this.getPixelsPerSecond(this.samples);
        this.containerWidth = this.wf.stage.getWidth();
        this.containerHeight = this.wf.stage.getHeight();
        this.time = this.getTotalTime();
        this.wf.zoom = new Zoom(this);
        this.draw(this.wf.zoom.currentZoom);
    }

    getMaxLevel() {
        let max = 0;
        this.samples.forEach(item => {
            let level = item.level * 1;
            if (level > max) {
                max = level;
            }
        });
        return max;
    }

    draw(zoomToSeconds = 0) {
        let zoom = this.getZoomFactor(zoomToSeconds);
        if (this.layers[zoomToSeconds]) {
            this.layers.forEach(layer => layer.hide());
            return this.layers[zoomToSeconds].show();
        }
        setTimeout(() => this.wf.playHeadLayer.moveToTop(), 1);
        let layerWidth = this.time * this.pixelsPerSecond * zoom;
        let self = this;
        this.layers[zoomToSeconds] = new Konva.Layer({
            draggable: true,
            dragBoundFunc: function(pos) {
                self.wf.em.emit('waveformDrag', this);
                let x = (layerWidth - self.containerWidth) < Math.abs(pos.x) ? -(layerWidth - self.containerWidth) : (pos.x < 0) ? pos.x : this.getAbsolutePosition().x;
                return {
                    x,
                    y: this.getAbsolutePosition().y
                }
            }
        });
        this.layers[zoomToSeconds].zoom = zoom;
        let background = new Konva.Rect({
            width: this.time * this.pixelsPerSecond * zoom,
            height: this.wf.stage.getHeight(),
            fill: '#f5f6f2'
        });
        let waveform = new Konva.Shape({
            opacity: 1,
            strokeWidth: 0,
            fill: '#6d9bd9',
            sceneFunc: function (context) {
                self.drawWaveform(context, this, zoom);
                //self.drawAxis(context);
            }
        });
        this.layers[zoomToSeconds].add(background);
        this.layers[zoomToSeconds].add(waveform);
        this.wf.stage.add(this.layers[zoomToSeconds]);
    }

    seek() {

    }

    drawWaveform(context, self, zoom) {
        context.beginPath();
        context.strokeStyle = '#c8d4ff';
        let count = 0;
        this.samples.forEach((d, index) => {
            context.lineTo(index * zoom, this.containerHeight - d.level);
            count = index;
        });
        context.lineTo(count * zoom, this.containerHeight);
        context.stroke();
        context.closePath();
        context.fillStrokeShape(self);
    }

/*    drawAxis(context) {
        let time = this.getTotalTime();
        let interval = 5; // interval of seconds
        let marksCount = Math.floor(time / interval);
        let step = this.containerWidth / marksCount;
        let x = 0;
        context.beginPath();
        context.strokeStyle = '#b6c1ff';
        for (; ;) {
            if (x >= this.containerWidth) {
                break;
            }
            x += step;
            context.moveTo(x + 0.5, 0);
            context.lineTo(x + 0.5, 4);
        }
        context.stroke();
        context.closePath();
    }*/

    getTotalTime() {
        return this.samples[this.samples.length - 1].time_ms / 1000; // in seconds
    }

    getZoomFactor(sec) { // sec
        sec = ((sec > this.time) || !sec) ? this.time : sec;
        let secondsPerContainer = this.containerWidth / this.pixelsPerSecond;
        let index = sec ? this.time / sec : 1;
        return (secondsPerContainer / this.time) * index;
    }

    // remove random steps from data, convert it to fixed step
    normalize(data) {
        let stepSize = 100; // in milliseconds
        let nextStep = -stepSize;
        let values = [];
        let res = [];
        let average = arr => arr.reduce((p, c) => p + c, 0) / arr.length;
        let actualStep = 0;
        data.forEach(d => {
            let time_ms = d.time_ms;
            let level = d.level * 1;
            values.push(level);
            let step = Math.abs((time_ms % stepSize) - time_ms);
            if (step > nextStep) {
                nextStep = step;
                let val = average(values);
                values = [];
                res.push({
                    time_ms: actualStep,
                    level: parseFloat(val.toFixed(2))
                });
                actualStep += stepSize;
            }
        });
        return res;
    }

    getPixelsPerSecond(data) {
        let count = 0;
        while(data[count].time_ms < 1000) {
            ++count;
        }
        return count;
    }

}
