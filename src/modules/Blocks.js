import Konva from "konva"
import Block from "./Block"
import events from "./Events"
import Playhead from "./Playhead";

/**
 * Class representing audio track blocks
 */
export default class Blocks {

    /**
     * block duration time in seconds
     */
    blockDuration;
    stage;
    trackTime;
    zoom;

    /**
     * @property {Konva.Layer} this.layer Blocks Konva layer
     */
    layer;

    /**
     * @property {Array.<Block>} this.blocks
     */
    blocks = [];

    /**
     * Create time blocks
     * @param {Object} options
     * @param {number} options.blockDuration This defines the block length in seconds
     * @param {number} options.containerWidth The client container width in pixels
     * @param {number} options.containerHeight The client container width in pixels
     * @param {number} options.trackTime Total time of the track in seconds
     */
    constructor(options) {
        Object.assign(this, options);

        this.containerWidth = this.stage.getWidth();
        this.containerHeight = this.stage.getHeight();


        this.pixelsInSecond = (this.containerWidth / this.blockDuration) * this.zoom;

        this.layer = new Konva.Layer({
            draggable: true
        });

        this.create();
        this.stage.add(this.layer);

        this.playhead = new Playhead(options);

        this.layer.on('dragmove click', event => {
            let c = this.keepInBounds();
            events.emit('stop');
            if (!c) {
                return;
            }
            events.emit('blocksMove', {
                event,
                layer: this.layer
            });
        });

        /**
         * Play animation for this.layer
         * @type {Konva.Animation}
         */
        this.animation = new Konva.Animation(frame => {
            let dist = this.containerWidth * frame.timeDiff / 1000 / this.blockDuration * this.zoom;
            this.setXPosition(-dist);
            if (!this.keepInBounds()) {
                this.animation.stop();
                events.emit('startPlayhead');
            }
        }, this.layer);

        events.on("zoomIn", () => {
            this.create(Math.random());
            this.layer.draw();
        });

        events.on("play", () => {
            this.animation.start();
        });

        events.on("stop", () => {
            this.animation.stop();
        });

        events.on("seek", time => {
            this.seek(time);
        });

        events.on("goStart", () => {
            this.seek(0);
        });

        events.on("goEnd", () => {
            this.seek(this.trackTime);
        });

    }

    /**
     * Prevents out of the container for this.layer
     * @return {boolean} If out of bounds (one of the bounds reached) then return false, otherwise true
     */
    keepInBounds() {
        let rect = this.layer.getClientRect();
        let posX = this.getXPosition();
        let x = (rect.width - this.containerWidth) < Math.abs(posX) ? - (rect.width - this.containerWidth) : (posX < 0) ? posX : 0;
        this.layer.position({x});
        return (x > (this.containerWidth - rect.width) && x < 0);
    }

    /**
     * Get current position of this.layer (the blocks layer)
     * @return {number}
     */
    getXPosition() {
        return this.layer.position().x;
    }

    /**
     * This creates array of Blocks
     */
    create() {
        this.layer.removeChildren();
        this.blocks = [];
        // get count of blocks
        let blocksCount = Math.ceil(this.trackTime / this.blockDuration);
        let width = this.pixelsInSecond * this.blockDuration;
        let lastWidth = this.pixelsInSecond * (this.trackTime - (blocksCount -1 ) * this.blockDuration);
        for (let i = 0; i < blocksCount; i++) {
            // end time of the block
            let start = this.blockDuration * i;
            // start time of the block
            let e = this.blockDuration * i + this.blockDuration;
            let end  = e > this.trackTime ? this.trackTime : e;
            let block = new Block({
                x: i * width,
                width: ((i + 1) === blocksCount) && lastWidth ? lastWidth : width,
                height: this.containerHeight,
                backgroundColor: getRandomColor(),
                timeRange: [start, end],
                trackTime: this.trackTime
            });
            this.blocks.push(block);
            this.layer.add(block.group);
        }
        console.log(this.blocks);
    }

    /**
     * Set X position for this.layer
     * @param delta - delta offset
     */
    setXPosition(delta) {
        let posX = this.getXPosition() + delta;
        this.layer.position({x: posX});
        this.layer.draw();
    }

    /**
     * This seeks to certain track time
     * @param time {number} Time in seconds
     */
    seek(time) {
        time = Math.abs(time * 1);

        let rect = this.layer.getClientRect();
        let trackWidth = rect.width;
        let blockWidth = this.blockDuration * this.pixelsInSecond;
        // calculate and set waveforms position
        let waveformsPosition = (time / this.trackTime) * trackWidth - blockWidth / 2;
        if (waveformsPosition <= 0) {
            waveformsPosition = 0;
        }
        if (waveformsPosition > (trackWidth - blockWidth)) {
            waveformsPosition = trackWidth - blockWidth;
        }
        this.layer.position({x: -waveformsPosition});
        this.layer.draw();
        // calculate and set the playhead position
        let containerDuration = this.containerWidth / this.pixelsInSecond;
        let playheadPosition;
        if (waveformsPosition === 0) {
            console.log('left');
            playheadPosition = time / this.blockDuration * this.pixelsInSecond;
        } else if (waveformsPosition >= (trackWidth - blockWidth)) {
            console.log('right');
            playheadPosition = (containerDuration - this.trackTime + time) * this.pixelsInSecond;
            playheadPosition = playheadPosition > this.containerWidth ? this.containerWidth : playheadPosition;
        } else {
            playheadPosition = this.containerWidth / 2;
        }
        this.playhead.setPosition(playheadPosition);
    }

    update() {

    }


}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
