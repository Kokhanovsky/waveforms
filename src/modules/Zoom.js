export default class Zoom {

    zooms = [0, 120, 60, 30, 10, 1]; // in seconds, 0 means actual time of the video

    currentZoom = this.zooms[0];

    constructor(wf) {
        this.peaks = wf;
    }

    zoomIn() {
        let index = this.zooms.indexOf(this.currentZoom);
        if ((index + 1) >= this.zooms.length) {
            return;
        } else {
            this.currentZoom = this.zooms[index + 1];
        }
        this.peaks.draw(this.currentZoom);
    }

    zoomOut() {
        let index = this.zooms.indexOf(this.currentZoom);
        if ((index) <= 0) {
            return;
        } else {
            this.currentZoom = this.zooms[index - 1];
        }
        this.peaks.draw(this.currentZoom);
    }



}
